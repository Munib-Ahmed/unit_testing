add_library(test_library STATIC test.cpp)
target_link_libraries(test_library gtest)

target_include_directories(test_library PRIVATE ${CMAKE_SOURCE_DIR}/include)

