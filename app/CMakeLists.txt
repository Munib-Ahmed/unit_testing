add_executable(${CMAKE_PROJECT_NAME} main.cpp)

target_link_libraries(${CMAKE_PROJECT_NAME} PRIVATE gtest student_library whitelist_library test_library)
include_directories(${CMAKE_SOURCE_DIR}/include)

