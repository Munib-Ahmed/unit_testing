#include <iostream>
#include "student.h"
#include "whitelist.h"
#include "test.h"
using namespace std;


int main()
{
    student student_1("maths", 1, 1.1, "i150-001", 1);
    student student_2("maths", 2, 2.2, "i150_002", 2);
    student student_3("maths", 3, 3.3, "i150-003", 3);
    student student_4("maths", 4, 4.4, "i150-004", 4);
    student student_5("maths", 5, 5.5, "i150-005", 5);
    student student_6("maths", 6, 6.6, "i150-006", 6);
    student student_7("maths", 7, 7.7, "i150-007", 7);
    student student_8("maths", 8, 8.8, "i150-008", 8);
    student student_9("maths", 9, 9.9, "i150-009", 9);
    student student_10("maths", 10, 10.10, "i150-010", 10);
    student student_11("maths", 11, 11.11, "i150-011", 11);
    student student_12("maths", 12, 12.12, "i150-012", 12);
    student student_13("maths", 13, 13.13, "i150-013", 13);
    student student_14("maths", 14, 14.14, "i150-014", 14);
    student student_15("maths", 15, 15.15, "i150-015", 15);

    whitelist list;
    list.add_to_whitelist("Ali 1", student_1);
    list.add_to_whitelist("Ali 2", student_2);
    list.add_to_whitelist("Ali 3", student_3);
    list.add_to_whitelist("Ali 4", student_4);
    list.add_to_whitelist("Ali 5", student_5);
    list.add_to_whitelist("Ali 6", student_6);
    list.add_to_whitelist("Ali 7", student_7);
    list.add_to_whitelist("Ali 8", student_8);
    list.add_to_whitelist("Ali 9", student_9);
    list.add_to_whitelist("Ali 10", student_10);
    list.add_to_whitelist("Ali 11", student_11);
    list.add_to_whitelist("Ali 12", student_12);
    list.add_to_whitelist("Ali 13", student_13);
    list.add_to_whitelist("Ali 14", student_14);
    list.add_to_whitelist("Ali 15", student_15);

    if (list.is_student_present("Ali 5"))
    {
        student *student_data = list.get_student_data("Ali 5");
        student_data->set_cgpa(5);
        student_data->set_subject_marks("physics", 88);
        student_data->set_subject_marks("physics", 56);
        student_data->print_all_marks();
        student_data->set_subject_marks("bio", 44);
        //float x =student_data->get_cgpa();
        //x=1000;
        //cout<<"x = "<<x<<endl;
        cout << "Student CGPA = " << student_data->get_cgpa() << endl;
        cout << "Student Roll no. = " << student_data->get_roll_no() << endl;
        cout << "Student marks in Maths = " << student_data->get_subject_marks("maths") << endl;
    }
    cout << endl
         << endl
         << "Another student" << endl;
    if (list.is_student_present("Ali 1"))
    {
        student *student_data = list.get_student_data("Ali 1");
        student_data->print_all_marks();
        cout << "Student CGPA = " << student_data->get_cgpa() << endl;
        cout << "Student Roll no. = " << student_data->get_roll_no() << endl;
        cout << "Student marks in Maths = " << student_data->get_subject_marks("maths") << endl;
    }
    cout << endl
         << endl
         << "Another student" << endl;
    if (list.is_student_present("Ali 100"))
    {
        student *student_data = list.get_student_data("Ali 100");
        student_data->print_all_marks();
        cout << "Student CGPA = " << student_data->get_cgpa() << endl;
        cout << "Student Roll no. = " << student_data->get_roll_no() << endl;
        cout << "Student marks in Maths = " << student_data->get_subject_marks("maths") << endl;
    }

    testing_suites();

}
