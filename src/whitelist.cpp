#include <iostream>
using namespace std;
#include "whitelist.h"

void whitelist::add_to_whitelist(const std::string &name, const student &s)
{
	student_data_list.push_back(s);
	student_record[name] = &student_data_list.back();
}

bool whitelist::is_student_present(const std::string &find_name) const
{
	if (student_record.find(find_name) != student_record.end())
	{
		cout << find_name << " is present in the list" << endl;
		return true;
	}
	else
	{
		cout << find_name << " is not present in the list" << endl;
		return false;
	}
}

student *whitelist::get_student_data(const std::string &find_name) const
{
	if (student_record.find(find_name) != student_record.end())
	{
		auto itr = student_record.find(find_name);
		return itr->second;
	}
	else
	{
		cout << "\t \t student not found" << endl;
		return NULL;
	}
}