cmake_minimum_required(VERSION 3.1.0)
project(unit_testing VERSION 1.0.0)

set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/build)

add_subdirectory(gtest)
add_subdirectory(src)
add_subdirectory(test)
add_subdirectory(app)