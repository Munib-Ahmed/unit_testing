#include "test.h"
#include "student.h"
#include "whitelist.h"
#include <iostream>
using namespace testing;
using namespace std;

TEST(whitelist, whitelist_test)
{
    student student_1("maths", 1, 1.1, "i150-001", 1);
    whitelist list;
    list.add_to_whitelist("Ali 1", student_1);
    ASSERT_EQ(list.is_student_present("Ali 1"), true);
    ASSERT_EQ(list.get_student_data("Al1 54"),nullptr); 
}

TEST(student, students_Test)
{
    student student_1("maths", 1, 1.1, "i150-001", 1);
    EXPECT_FLOAT_EQ(student_1.get_cgpa(), 1.1);
}

void testing_suites()
{
    InitGoogleTest();
    int no_of_errors = RUN_ALL_TESTS();
    cout<<"Total number of Errors in GTest = "<<no_of_errors<<endl;
}
