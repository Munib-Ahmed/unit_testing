#include <iostream>
#include <map>
#include <string>
#include <iterator>
#include "student.h"
using namespace std;

student::student(const string &course, const int &marks, const float &gpa, const string &roll_no, const int &age)
{
	set_subject_marks(course, marks);
	set_age(age);
	set_cgpa(gpa);
	set_roll_no(roll_no);
}
student::~student()
{
}

int student::get_subject_marks(const std::string &subject) const
{
	if (result.find(subject) == result.end())
	{
		cout << "\t \t course not found" << endl;
		return -1;
	}
	else
	{
		auto itr = result.find(subject);
		return itr->second;
	}
}

void student::set_subject_marks(const std::string &subject, const int &marks)
{
	result[subject] = marks;
}

void student::print_all_marks() const
{
	cout << '\t' << "Subjects " << '\t' << '\t' << " Marks" << endl;
	for (auto itr = result.begin(); itr != result.end(); ++itr)
	{
		cout << '\t' << itr->first << '\t' << '\t' << itr->second << endl;
	}
}

void student::set_cgpa(const float &gpa)
{
	st_object.cgpa = gpa;
}

void student::set_age(const int &age)
{
	st_object.age = age;
}

void student::set_roll_no(const string &roll_no)
{
	st_object.roll_no = roll_no;
}

const float& student::get_cgpa() const
{
	return st_object.cgpa;
}

const int& student::get_age() const
{
	return st_object.age;
}

const string& student::get_roll_no() const
{
	return st_object.roll_no;
}
